---
type: blog
title: SOAFEE Accelerates Autonomous Race Car Development
description: Discover how the SOAFEE SIG is collaborating with academia to accelerate autonomous race car development
date: 2022-05-04
banner:
  background: banner/banner1.jpg
---

import Image from 'next/legacy/image'
import Architecture from './Picture1.svg'
import DevProcess from './Picture2.svg'
import AutoTuning from './Picture3.svg'
import Migration from './Picture4.svg'

# SOAFEE Accelerates Autonomous Race Car Development

By: Girish Shirasat (Arm), Felix Fent (TUM), Stefano Marzani (AWS)

## Introduction

The autonomy technology juggernaut is stronger than ever and even though the promises of the fully L4/L5 capable vehicle are something that we still need to see on the roads at production scale, the pace of innovation to solve some of the most complex problems is higher than ever. Many of these innovations are being driven by both the commercial OEMs and academia. If history has taught us anything, it's that some of the best innovations come out of the bright and passionate minds in academia. When you combine this brilliance with something like an autonomous vehicle race as conducted by the Indy Autonomous Challenge (IAC) [1], where some of the best universities around the world compete with each other to build fastest autonomous vehicles racing at more than 250 km/h for a prize money of 1.5 million USD, there cannot be a more fertile ground for innovation than this. One could say that IAC is a possible successor to the DARPA Grand Challenge which - autonomy connoisseurs will know - triggered most of the autonomy trend we see today. The first of these races took place on the Indianapolis Motor Speedway in October 2021 which was won by Technical University of Munich (TUM) from Germany.

In this blog, we provide a high-level overview of the  autonomous stack used by TUM in the IAC race along with their adoption of state of the art cloud-native development process. In addition, we identify some of the key scaling issues that they face due to their current computing infrastructure and how working with Arm/AWS through the Scalable Open Architecture For Embedded Edge (SOAFEE, [www.soafee.io](https://soafee.io "SOAFEE Website")), we intend to address them.

## Cloud-Native In Autonomy

In the new age of software defined vehicles, features in a car will no longer be fixed functions delivered during the manufacture of the vehicle in the assembly line but will be developed, deployed and monetized across the life cycle of the vehicle. Software enabling these features will be the key differentiator for an OEM and the role of software developer becomes more important than ever in the entire automotive value chain. Increasing  developer effectiveness has a direct impact on the bottom line of the OEM and its entire supply chain. Cloud-Native is presented as one of the design patterns to improve  developer effectiveness and has been successfully deployed in the enterprise domain. It is now making its way in automotive. [The cloud-native approach to the software defined car](https://community.arm.com/arm-community-blogs/b/embedded-blog/posts/cloud-native-approach-to-the-software-defined-car?_ga=2.84349662.63983164.1633923986-188627514.1623282169&_gac=1.188151258.1631741153.Cj0KCQjws4aKBhDPARIsAIWH0JWzFX7JhWfY12ecutW_Gaiy3HwXQ1QWT1HbMuvrAnTdtsTSdk57dzAaAq7DEALw_wcB) blog [2] provides an overview of what it means to apply a cloud-native approach in the context of automotive while [Accelerating Software-Defined Vehicles through Cloud-To-Vehicle Edge Environmental Parity](https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-aws-edge-environmental-parity-wp.pdf) [3] describes the impact of cloud-to-automotive-edge environmental parity on cloud-native automotive system development. Additionally [How the SOAFEE Architecture Brings A Cloud-Native Approach To Mixed Critical Automotive Systems](https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-aws-edge-environmental-parity-wp.pdf) [7] blog provides an overview of how Scalable Open Architecture For Embedded Edge (SOAFEE, [www.soafee.io](https://soafee.io)) intends to address some of the key challenges in adopting cloud native in mixed critical workload development in automotive.
Arm, working alongside Autoware Foundation, started the Open AD Kit initiative [6] to democratize cloud-native devops in autonomy enabled through SOAFEE.

## Cloud-Native Autonomous Race Car Development

As another step in this journey and to foster the innovations in academia, technology leaders Arm and AWS are jointly collaborating with the Technical University of Munich (TUM),  the winner of the first IAC race, to accelerate their cloud-native development environment used for their autonomy stack with end-to-end Arm based environmental parity. Some of the exciting video footage of the IAC race is available for [public viewing](https://www.youtube.com/watch?v=F4rQK9Sp25s) [8].

#### TUM Autonomy Software Architecture And Development Environment

<Architecture style={{width: '100%', height: '100%'}}/>

Figure 1 : TUM Autonomy Software Architecture

Figure 1 shows the architecture of the software stack that runs in TUM's autonomous racing cars. Like most autonomous software stacks, it comprises the compute phases of sensing, localization, perception, prediction, planning and control [9]. It is optimized to handle unstructured environments with high prediction uncertainties and very short reaction times as one would expect for operating at the limits of vehicle dynamics.

To develop such a complex software, TUM already leverages the best practices of cloud-native DevOps as shown in Figure 2.

<DevProcess style={{width: '100%', height: '100%'}}/>

Figure 2 : Development Process
				
The development process incorporates a fully-fledged CI/CD framework with unit and integration tests including scenario-based software in the loop (SiL) simulation as well as a dedicated hardware in the loop (HiL) simulation. Additionally, as part of the continuous deployment process, the infrastructure includes deploying software components onto the vehicle and runtime monitoring of its operation. This enables an infrastructure for agile software deployment and reduces the developer feedback time by an order of magnitude, thus providing a basis for continuous system optimizations.
				
A unique feature of the TUM autonomy system development is its automated parameter tuning process as shown in Figure 3.

<AutoTuning style={{width: '100%', height: '100%'}}/>

Figure 3 : Automated Parameter Tuning
		
As part of this tuning process, explicit autonomy stack parameters are tuned using the Nevergrad [10] gradient free optimization algorithm providing an infrastructure for continuous system performance improvement.

## TUM Cloud-Native Development Environment

For the development of such a complex software stack along with the described CI/CD infrastructure, it is necessary to have a compute heavy and elastic cloud based environment. Additionally, when this stack is deployed in the vehicle, there needs to be an equally compute rich and power optimised ECU.  

<Migration style={{width: '100%', height: '100%'}}/>

Figure 4 : Migration to an Arm-based SOAFEE Development Environment 

As shown in Figure 4, the existing TUM development infrastructure is based on a x86 compute architecture, which consists of a on-prem cloud cluster, a HIL and a HPC (ECU). There are a few limitations in this infrastructure which limits scale. Some of them are listed below:

- Due to limited compute in the current on-prem cloud, it becomes difficult for  scaling development as the number of students working on this system increases.
- The performance of the automated tuning process is limited by the number of parallel optimization threads it can run on the current on-prem cluster with different parameter sets. 
- The vehicle ECU needs to be power and performance optimised with a path to have mixed critical design. For such system designs, Arm is one of the key technology providers for automotive OEMs. For the TUM stack to evolve to the next phase of development, it needs to transition to Arm based ECUs.
- As described in [3], the level of environmental parity between cloud and automotive edge directly impacts the developer effectiveness. Moving the ECUs to Arm would ideally call for moving to Arm based cloud instances based on the AWS Graviton instances to leverage environmental parity benefits.

To address some of the above limitations, TUM in collaboration with Arm and AWS are working on following key aspect of the development environment:

- Transition from the current on-prem cluster to a A Graviton based cloud environment.
- Transition to Arm based central compute systems [5].
- Adopt SOAFEE based deployment infrastructure enabling a path to mixed critical workload development and deployment.

The intent is to open source the infrastructure to act as an innovation hub for the academia community and form one of the SOAFEE based reference implementations.

## Conclusion

Autonomy is quite an exciting technology segment which is bringing in a level of software complexity that the automotive industry has not seen. Cloud-Native design paradigm has been used in the enterprise domain to manage software complexity over the past decade and is making its way into automotive. With automotive having unique platform requirements due to the mixed critical nature of the software, there are challenges to the adoption of the existing cloud-native technologies when it comes to development and deployment at production scale. Arm and AWS as the leading technology provider to the automotive ecosystem through initiatives like SOAFEE are looking at addressing these challenges, by fostering strong collaboration with academia ecosystem partners like Technical University of Munich so that we can enable some of the best minds to work on the most complex problems in the automotive space.

## References

1. IAC, [Indy Autonomous Challenge](https://www.indyautonomouschallenge.com/)
1. Girish Shirasat, [The cloud-native approach to the software defined car](https://community.arm.com/arm-community-blogs/b/embedded-blog/posts/cloud-native-approach-to-the-software-defined-car?_ga=2.84349662.63983164.1633923986-188627514.1623282169&_gac=1.188151258.1631741153.Cj0KCQjws4aKBhDPARIsAIWH0JWzFX7JhWfY12ecutW_Gaiy3HwXQ1QWT1HbMuvrAnTdtsTSdk57dzAaAq7DEALw_wcB)
1. Girish Shirasat, Stefano Marzani [Accelerating Software-Defined Vehicles through Cloud-To-Vehicle Edge Environmental Parity](https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-aws-edge-environmental-parity-wp.pdf)
1. Scalable Open Architecture For Embedded Edge  https://soafee.io/
1. Arm AVA Centralised Compute Platform  https://www.adlinktech.com/en/soafee
1. Autoware Foundation Open AD Kit https://www.autoware.org/autoware-open-ad-kit
1. Matt Spencer, How the [SOAFEE Architecture Brings A Cloud-Native Approach To Mixed Critical Automotive Systems](https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-scalable-open-architecture-for-embedded-edge-soafee.pdf)
1. TUM IAC 2021 final lap, https://www.youtube.com/watch?v=F4rQK9Sp25s
1. Alexander Wischnewski et al., [Indy Autonomous Challenge - Autonomous Race Cars at the Handling Limits](https://arxiv.org/abs/2202.03807)
1. J. Rapin and O. Teytaud, [Nevergrad - A gradient-free optimization platform](https://GitHub.com/FacebookResearch/Nevergrad)

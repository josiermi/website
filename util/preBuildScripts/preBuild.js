
import generateMembers from './generateMembers.js';
import generateBanners from './generateBanners.js';

let outputDir = './autogen'

generateMembers(outputDir);
generateBanners(outputDir);